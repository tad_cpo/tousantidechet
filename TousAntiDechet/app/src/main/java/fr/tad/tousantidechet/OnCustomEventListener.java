package fr.tad.tousantidechet;

// Interface with a callback custom function
public interface OnCustomEventListener {
    public void onEvent();
}
