package fr.tad.tousantidechet;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Announce {

    private final String id;
    private String authorId;
    private String type;
    private String description;
    private double latitude;
    private double longitude;

// ------------------------------------- Constructor -----------------------------------------------

    public Announce(String authorId,String type, String description, double latitude, double longitude) {

        //get a unique uId from firebase
        FirebaseDatabase database = FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/");
        DatabaseReference myRef = database.getReference();
        //getKey method provides the unique id
        String key = myRef.child("Announces").push().getKey();



        this.id = key;
        this.authorId = authorId;
        this.type = type;
        this.description = description;
        this.latitude = latitude;
        this.longitude = longitude;

    }

    //Default constructor
    public Announce() {
        this.id = null;

    }

// -------------------------------------- Getters --------------------------------------------------

    public String getId() {
        return id;
    }

    public String getAuthorId() {
        return authorId;
    }

    public String getType() {
        return type;
    }

    public String getDescription() {
        return description;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

// -------------------------------------- Setters --------------------------------------------------


    public void setType(String type) {
        this.type = type;
    }

    public void setDescription(String description) { this.description = description; }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

}
