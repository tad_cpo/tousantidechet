package fr.tad.tousantidechet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.os.Bundle;
import android.text.Layout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.maps.model.LatLng;

public class SeeAnnouncesActivity extends AppCompatActivity  {

    public static LatLng latLng=new LatLng(1,1);
    public static MutableLiveData<Boolean> isClicked = new MutableLiveData<Boolean>();
    private CardView create_announce_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        isClicked.setValue(false);

        setContentView(R.layout.activity_see_announces);
        RecyclerView recyclerView= findViewById(R.id.recyclerView);

        // Load the database and display each announce in the recycler view
        AnnounceStorageManager announceStorageManager=AnnounceStorageManager.getInstance();

        announceStorageManager.setCustomEventListener(new OnCustomEventListener() {
            @Override
            public void onEvent() {
                if(!announceStorageManager.getAnnounces().isEmpty()){

                    recyclerView.setAdapter(new AnnounceAdapter(announceStorageManager.getAnnounces()));


                }
                else{
                    Toast.makeText(getApplicationContext(),"announces list is empty",Toast.LENGTH_SHORT).show();
                    }
            }
        });

        // Listen for any modification in Announces database
        announceStorageManager.updateAnnounces();

        // Listen if isClicked is modified
        isClicked.observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(Boolean aBoolean) {
                if(isClicked.getValue()){
                    startActivity(new Intent(getApplicationContext(),MapsActivitySee.class));
                    overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                    finish();
                }
            }
        });


        create_announce_button = findViewById(R.id.add_announce_nav_button);
        // Listen if the create announce button is clicked
        create_announce_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),CreateAnnounceActivity.class));
                overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_up);
                finish();
            }
        });



    }



    @Override
    public void onBackPressed(){
        //if back button is pressed: redirect back to main activity
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        //Precise the animation when switch activity
        overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_up);
        //Close the settings activity
        finish();
    }

}

