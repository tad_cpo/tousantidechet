package fr.tad.tousantidechet;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;


// Not finished

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String CANAL = "MyNotifCanal" ;

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        //récupérer le contenu de la notification
        String myMessage = remoteMessage.getNotification().getBody();

        //afficher en console le message
        Log.d("FirebaseMessage", "Vous venez de recevoir une notification : " + myMessage );

        //action
        //rediriger vers une page weg (http)
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://graven.yt")); // A MODIFIER VERS LIEN APPLI
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        //créer une notification
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, CANAL);
        notificationBuilder.setContentTitle("Ma nouvelle notification");
        notificationBuilder.setContentText(myMessage);

        //ajouter l'action
        notificationBuilder.setContentIntent(pendingIntent);

        //créer la vibration
        long[] vibrationPattern = {500,1000};
        notificationBuilder.setVibrate(vibrationPattern);

        //insérer une icône pour la notification
        notificationBuilder.setSmallIcon(R.drawable.notification);

        //envoie de la notification
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            String channelId = getString(R.string.notification_channel_id);
            String channelTitle = getString(R.string.notification_channel_title);
            String channelDescription = getString(R.string.notification_channel_desc);
            NotificationChannel channel = new NotificationChannel(channelId, channelTitle,NotificationManager.IMPORTANCE_DEFAULT);
            channel.setDescription(channelDescription);
            notificationManager.createNotificationChannel(channel);
            notificationBuilder.setChannelId(channelId);
        }

        notificationManager.notify(1, notificationBuilder.build());
    }
}
