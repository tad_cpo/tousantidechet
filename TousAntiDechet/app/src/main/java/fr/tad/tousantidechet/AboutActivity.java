package fr.tad.tousantidechet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class AboutActivity extends AppCompatActivity {

    private ImageView back_icon;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        back_icon=(ImageView) findViewById(R.id.back_icon);

        back_icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //Every time main_icon is clicked :

                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                //Close the current Activity
                finish();
            }
        });
    }

    @Override
    //Listen if back button is pressed
    public void onBackPressed(){

        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        //Close the current Activity
        finish();


    }
}