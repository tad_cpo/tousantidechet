package fr.tad.tousantidechet;

import android.webkit.ConsoleMessage;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Document;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.callback.Callback;

//This class is used to link the local storage and the Announces database (Firebase real time data base)
public class AnnounceStorageManager {

    private List<Announce> announces;
    private static AnnounceStorageManager announceStorageManager;
    private  DatabaseReference myRef=FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference("Announces");
    private OnCustomEventListener listener;


//-------------------------------------- Constructor -----------------------------------------------

    private AnnounceStorageManager(){
        announces = new ArrayList<Announce>();
    }

    public static AnnounceStorageManager getInstance()
    {
        if ( announceStorageManager== null)
        {
            announceStorageManager = new AnnounceStorageManager();
        }
        return announceStorageManager;
    }

//--------------------------------------- Getters --------------------------------------------------

    public List<Announce> getAnnounces(){
        return announces;
    }

    public DatabaseReference getMyRef() {
        return myRef;
    }

    public OnCustomEventListener getListener(){return listener;}


// ------------------------------ Set the Custom Listener ------------------------------------------

   public void setCustomEventListener(OnCustomEventListener eventListener){
       this.listener=eventListener;
    }


// ----------------------------- Database modification methods -------------------------------------


    //Add a new announce in the database
    public static void addAnnounce(Announce announce){
        FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/")
                .getReference("Announces").child(announce.getId()).setValue(announce);
        MapsActivityAdd.lat=0;MapsActivityAdd.lng=0;
    }

    //Delete a user in the database using his unique id
    public static void deleteAnnounce(Announce announce){
        FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/")
                .getReference("Announces").child(announce.getId()).removeValue();
    }

// ------------------------------- Local Array modification methods --------------------------------


    public void updateAnnounces(){

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Clear the local array to rewrite the entire database
                announces.clear();

                //Get every items in the database
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    Announce value = ds.getValue(Announce.class);

                    //and add them in the local array
                    announces.add(value);
                }
                //Call onEvent when the database has been successfully copied locally
                // onEvent method should be Override in the Activity updateAnnounces method is called
                if(listener!=null){
                    listener.onEvent();}
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
