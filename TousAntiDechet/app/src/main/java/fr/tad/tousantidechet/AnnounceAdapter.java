package fr.tad.tousantidechet;

import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.google.android.gms.maps.model.LatLng;
import com.google.firebase.database.core.Context;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class AnnounceAdapter extends RecyclerView.Adapter {
    private List<Announce> announces;

// --------------------------------- Constructor ---------------------------------------------------

    public AnnounceAdapter(List<Announce> announces) {
        super();
        this.announces = new ArrayList<Announce>();

        //static method to get chosen radius
        User.getCurrentUserRadius();

        //We create the adapter only with the announces within the chosen radius
        for (Announce a : announces) {
            // Calculate distance between the user position and the announce, using the Earth radius
            double distance = getAnnounceDistance(a);

            if (Math.abs(distance) < User.mRadius) {
                this.announces.add(a);
            }
        }

    }

// ---------------------------------- Adapter default functions ------------------------------------

    // We associate a layout and the adapter in a ViewHolder
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_announce, parent, false);
        return new ViewHolder(view);
    }

    // What we do in each ViewHolder, modify for each announce
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        Announce currentAnnounce = announces.get(position);

        DecimalFormat df = new DecimalFormat("0.0"); // Format to display on the app
        double distance=getAnnounceDistance(currentAnnounce);

        // Create a custom ViewHolder from a RecyclerView.ViewHolder object
        ViewHolder holder1 = (ViewHolder) holder;

        // Set the announce data to display
        holder1.getType().setText(currentAnnounce.getType());
        holder1.getDescription().setText(currentAnnounce.getDescription());
        holder1.getDistance().setText(df.format(distance) + "km");


        holder1.getMap_display_icon().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SeeAnnouncesActivity.latLng = new LatLng(currentAnnounce.getLatitude(), currentAnnounce.getLongitude());
                SeeAnnouncesActivity.isClicked.setValue(true);
                System.out.println(SeeAnnouncesActivity.latLng);

            }
        });
    }


    @Override
    public int getItemCount() {
        return announces.size();
    }

// ----------------------------------- Others functions --------------------------------------------

    public double getAnnounceDistance(Announce currentAnnounce) {

        double distance = (6371
                * Math.acos(
                Math.sin(Math.PI / 180 * MainActivity.currentLocation.latitude)
                        * Math.sin(Math.PI / 180 * currentAnnounce.getLatitude())

                        + Math.cos(Math.PI / 180 * MainActivity.currentLocation.latitude)
                        * Math.cos(Math.PI / 180 * currentAnnounce.getLatitude())
                        * Math.cos((Math.PI / 180 * MainActivity.currentLocation.longitude - Math.PI / 180 * currentAnnounce.getLongitude()))));

        return distance;
    }
}