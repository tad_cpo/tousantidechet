package fr.tad.tousantidechet;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.ValueEventListener;


//This Activity is the app main menu and welcome page once you are registered
//It allows the user to navigate to the others activities
//Every other activities allows you to come back to this one

public class MainActivity extends AppCompatActivity {


    private ImageView settings_icon,main_icon,info_icon,alert_icon;
    Button create_announce_button,see_announces_button;

    private int back_pressed_counter =0;

    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private boolean isLocationEnable = false;

    public static LatLng currentLocation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //When the Activity starts:
        super.onCreate(savedInstanceState);

        //Link the layout activity_main to display it
        setContentView(R.layout.activity_main);

        // ----------------------------- Get user location -----------------------------------

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);

        // Check the permissions
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this, new String[]{FINE_LOCATION, COARSE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
            return;
        } else {
            isLocationEnable = true;

        }

        if (isLocationEnable) {


            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                            }
                        }
                    });
        }



// ------------------------- Interaction with graphic items --------------------------------------

        //Get the graphic item main_icon to link and listen for interaction
        main_icon=(ImageView) findViewById(R.id.main_icon);

        //Listen for a click on the item main_icon
        main_icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //Every time main_icon is clicked :
                back_pressed_counter=0;
            }
        });

        //Get the graphic item settings_icon to link and listen for interaction
        settings_icon = findViewById(R.id.settings_icon);

        //Listen for a click on the item settings_icon
        settings_icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //Every time main_icon is clicked :
                back_pressed_counter=0;

                //Start the SettingsActivity
                startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                //Close the current Activity
                finish();


            }
        });

        //Get the graphic item main_icon to link and listen for interaction
        info_icon=(ImageView) findViewById(R.id.info_icon);

        //Listen for a click on the item main_icon
        info_icon.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //Every time main_icon is clicked :
                back_pressed_counter=0;
                startActivity(new Intent(getApplicationContext(), AboutActivity.class));
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                //Close the current Activity
                finish();
            }
        });

        alert_icon=findViewById(R.id.alert_icon);
        alert_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                back_pressed_counter=0;
                startActivity(new Intent(getApplicationContext(), AlertActivity.class));
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                //Close the current Activity
                finish();
            }
        });

        create_announce_button=findViewById(R.id.create_announce_button);
        //Listen for a click on the item settings_icon
        create_announce_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //Every time create_announce_button is clicked :
                back_pressed_counter=0;

                //Start the CreateAnnounceActivity
                startActivity(new Intent(getApplicationContext(), CreateAnnounceActivity.class));
                overridePendingTransition(R.anim.slide_in_down,R.anim.slide_out_up);
                //Close the current Activity
                finish();
            }
        });

        see_announces_button=findViewById(R.id.see_announce_button);

        see_announces_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back_pressed_counter=0;

                startActivity(new Intent(getApplicationContext(),SeeAnnouncesActivity.class));
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
                //Close the current Activity
                finish();
            }
        });



    }


// ------------------------------- Navigation Interactions ----------------------------------------


    @Override
    //Listen if back button is pressed
    public void onBackPressed(){
        //Every time the back button is pressed

        //You need to double press the back button to close the app (the counter is reset when you click elsewhere)
        back_pressed_counter++;
        if(back_pressed_counter==1){
            //If back is pressed once: display a message to explain you need to double press back to quit
            Toast.makeText(getApplicationContext(),"Appuyez encore pour quitter",Toast.LENGTH_SHORT).show();
        }

        if(back_pressed_counter==2){
            //If back is pressed twice: close the activity (close the app)
            finish();
        }
    }


}


