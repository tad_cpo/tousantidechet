package fr.tad.tousantidechet;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


//This activity allows you, interacting with graphic elements, to register creating a new account
//If you are already registered you can navigate back to login activity

public class RegisterActivity extends AppCompatActivity {

    FirebaseAuth myAuth;
    Spinner region_spinner,age_spinner;
    private TextInputLayout email_input,password_input,password2_input,name_input;
    Button register_button;
    TextView already_registered_text;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Link with graphic elements
        setContentView(R.layout.activity_register);

        myAuth = FirebaseAuth.getInstance();
        region_spinner = findViewById(R.id.region_spinner);
        name_input = findViewById(R.id.name_input);
        age_spinner = findViewById(R.id.age_spinner);
        email_input = findViewById(R.id.email_input);
        password_input = findViewById(R.id.password_input);
        password2_input = findViewById(R.id.password2_input);
        register_button = findViewById(R.id.register_button);
        already_registered_text = findViewById(R.id.already_registered_text);


// ------------------------------ Interactions whit graphic items ----------------------------------


        //Create the spinners from arrays in resources
        ArrayAdapter<CharSequence> regionSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.regions_array, android.R.layout.simple_expandable_list_item_1);
        regionSpinnerAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        region_spinner.setAdapter(regionSpinnerAdapter);

        ArrayAdapter<CharSequence> ageSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.age_array, android.R.layout.simple_expandable_list_item_1);
        ageSpinnerAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        age_spinner.setAdapter(ageSpinnerAdapter);

        String[] spinnerValues = {"", ""};

        //Get the selected items in the spinners
        region_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Set the color of the 1st element (only one to be displayed)
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                //Set the text size
                //((TextView) parent.getChildAt(0)).setTextSize(int);

                //get the selected item
                String item = parent.getItemAtPosition(position).toString();

                //if the selected item is NOT the first one, store item in spinnerValues
                if (!Objects.equals(item, parent.getItemAtPosition(0).toString())) {
                    spinnerValues[0] = item;
                }
                //If the selected item is the first of the spinner, don t store it (it is the instruction 'Select your region')
                else {
                    spinnerValues[0] = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //If nothing is selected, nothing to store
                spinnerValues[0] = "";
            }
        });


        //Exactly the same as region spinner
        age_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);
                String item = parent.getItemAtPosition(position).toString();
                if (!Objects.equals(item, parent.getItemAtPosition(0).toString())) {
                    spinnerValues[1] = item;
                } else {
                    spinnerValues[1] = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                spinnerValues[1] = "";
            }
        });


        //When register button is clicked
        register_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                name_input.setErrorEnabled(false);
                email_input.setErrorEnabled(false);
                password_input.setErrorEnabled(false);
                password2_input.setErrorEnabled(false);

                //get the inputs
                String name = name_input.getEditText().getText().toString().trim();
                String email = email_input.getEditText().getText().toString().trim();
                String password = password_input.getEditText().getText().toString().trim();
                String password2 = password2_input.getEditText().getText().toString().trim();

                //----------- Test the inputs -----------
                //Test if any information is missing
                if (TextUtils.isEmpty(name)) {
                    name_input.setError("Entrez un pseudo");
                    return;
                }


                if (TextUtils.isEmpty(email)) {
                    email_input.setError("Entrez un email");
                    return;
                }


                if (TextUtils.isEmpty(password)) {
                    password_input.setError("Entrez un mot de passe");
                    return;
                }

                if (password.length() < 6) {
                    password_input.setError("Le mot de passe doit contenir au moins 6 caractères");
                    return;
                }

                if (password2.length() < 6) {
                    password2_input.setError("Le mot de passe doit contenir au moins 6 caractères");
                }

                if (!Objects.equals(password, password2)) {
                    password2_input.setError("Entrez un mot de passe identique");
                }

                if (TextUtils.isEmpty(spinnerValues[0])) {
                    Toast.makeText(RegisterActivity.this, "Sélectionnez votre région", Toast.LENGTH_SHORT).show();


                    if (TextUtils.isEmpty(spinnerValues[1])) {
                        Toast.makeText(RegisterActivity.this, "Sélectionnez votre age", Toast.LENGTH_SHORT).show();
                    }
                }

                //If there is no missing info then create the new account
                else {

                    //---------------- Database interactions --------------------


                    //Verify if an account with these email and password exists
                    myAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                //if this account already exists:
                                //log out the user
                                myAuth.signOut();
                                //display a message
                                Toast.makeText(RegisterActivity.this, "Un compte avec cet email existe déjà", Toast.LENGTH_SHORT).show();
                                email_input.setError("Entrez un autre email");
                                return;

                            } else {
                                //Create a user in the firebase authentication database
                                myAuth.createUserWithEmailAndPassword(email, password2);

                                //Then the new user logs in automatically
                                myAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                                    @Override
                                    public void onComplete(@NonNull Task<AuthResult> task) {
                                        if (task.isSuccessful()) {

                                            //Display a message to inform the user
                                            Toast.makeText(RegisterActivity.this, "Votre compte a bien été créé", Toast.LENGTH_SHORT).show();

                                            //Create a local user with the id created by Firestore
                                            //Created after the log in because the you need the user to be logged in to get his firestore id
                                            User user = new User(name, myAuth.getCurrentUser().getUid(), spinnerValues[1], spinnerValues[0]);

                                            //Add this user to the real time database (which is different from authentication database)
                                            //This user will be added automatically to our local array in main activity
                                            UserStorageManager.addUser(user);

                                            //Start the main activity
                                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                            //Precise the animation
                                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                                            //close register activity
                                            finish();

                                        } else {
                                            //If log in failed, display an error message, it means that the email has already been used with another password
                                            // or email syntax is not valid

                                            email_input.setError("Entrez un email valide");
                                        }

                                    }
                                });
                            }
                        }
                    });
                }
            }
        });


// ----------------------------------- Navigation Interactions -------------------------------------

        //Listen for a click on item
        already_registered_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //redirects you back to login activity
                startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                //Precise the animation
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                //close register activity
                finish();

            }
        });
    }

    //This one cannot be in the onCreate method
    @Override
    public void onBackPressed() {
        //redirected back to login activity
        startActivity(new Intent(getApplicationContext(),LoginActivity.class));
        //Precise the animation
        overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
        //close register activity
        finish();
    }
}