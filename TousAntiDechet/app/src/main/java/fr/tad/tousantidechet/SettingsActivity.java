package fr.tad.tousantidechet;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.w3c.dom.Text;

import java.util.Objects;

//This activity allows you to modified your preferences, and your app settings
// It is also where you can log out or delete your account

public class SettingsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Link the associated layout
        setContentView(R.layout.activity_settings);

        ImageView back_icon;
        Button logout_button,delete_account_button,yes_notifications_button, no_notifications_button;
        Spinner radius_spinner;

// ----------------------- Interactions with graphic items -----------------------------------------

        //Listen for click on back_icon
        back_icon=findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //redirect to main activity if click
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                //Precise animation when switching activity
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                finish();
            }
        });

        //Listen for click on logout_button
        logout_button=findViewById(R.id.logout_button);
        logout_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Log out the user using firebase methods
                FirebaseAuth.getInstance().signOut();

                //Display a message to inform the user
                Toast.makeText(SettingsActivity.this, "Vous avez bien été déconnecté", Toast.LENGTH_SHORT).show();

                //Redirect to login activity (which is the launcher activity)
                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                //Precise animation
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                finish();
            }
        });

        //Listen for click on delete_account_button
        delete_account_button=findViewById(R.id.delete_account_button);
        delete_account_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Build a dialog window to confirm the deletion of the account
                // setting the details of the displayed window with AlertDialog.Builder methods
                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SettingsActivity.this);

                //set the title of the window
                dialogBuilder.setTitle("Confirmer la suppression")
                        //set the message to display
                        .setMessage("Etes vous sur de supprimer votre compte ?")
                        //set the text on the positive button then listen for a click on it
                        .setPositiveButton("Oui", new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog,int i){
                                // If user confirms the deletion of his account:

                                //get the unique id of the current user
                                String currentUserId=FirebaseAuth.getInstance().getCurrentUser().getUid();

                                //delete the current user from the Authetification database
                                FirebaseAuth.getInstance().getCurrentUser().delete();

                                //log out the user
                                FirebaseAuth.getInstance().signOut();

                                //delete the user from the real time database
                                UserStorageManager.deleteUser(currentUserId);

                                //Display a message to inform the user
                                Toast.makeText(getApplicationContext(), "Votre compte à bien été supprimé", Toast.LENGTH_SHORT).show();

                                //redirect to login activity
                                startActivity(new Intent(getApplicationContext(),LoginActivity.class));
                                //Precise animation
                                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                //close activity
                                finish();
                            }

                        })
                        //set text on negative button then don t listen for a click on it
                        //(if negative button is clicked, it will automatically close the dialog window
                        .setNegativeButton("Non",null)

                        //set an icon to be displayed on the window
                        .setIcon(R.drawable.alert_icon);

                //create and show the window (still if delete_account_button has been clicked)
                AlertDialog alertDialog = dialogBuilder.create();
                alertDialog.show();
            }
        });

        //Listen for click on yes_notifications_button
        yes_notifications_button=findViewById(R.id.yes_notifications_button);
        yes_notifications_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //Interaction between database and buttons
                DatabaseReference myRef = FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
                //Get current user uId
                String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                //Enable the notifications
                myRef.child("Users").child(authorId).child("notification_activity").setValue(true);
                Toast.makeText(getApplicationContext(),"Notifications activées",Toast.LENGTH_SHORT).show();
            }
        });

        //Listen for click on no_notifications_button
        no_notifications_button=findViewById(R.id.no_notifications_button);
        no_notifications_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Interaction between database and buttons
                DatabaseReference myRef = FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
                //Get current user uId
                String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                //Enable the notifications
                myRef.child("Users").child(authorId).child("notification_activity").setValue(false);
                Toast.makeText(getApplicationContext(),"Notifications désactivées",Toast.LENGTH_SHORT).show();
            }
        });


        radius_spinner=findViewById(R.id.radius_spinner);

        ArrayAdapter<CharSequence> radiusSpinnerAdapter = ArrayAdapter.createFromResource(this,
                R.array.radius_array, android.R.layout.simple_expandable_list_item_1);
        radiusSpinnerAdapter.setDropDownViewResource(android.R.layout.select_dialog_singlechoice);
        radius_spinner.setAdapter(radiusSpinnerAdapter);



        //Get the selected items in the spinners
        radius_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //Set the color of the 1st element (only one to be displayed)
                ((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);

                //Set the text size
                //((TextView) parent.getChildAt(0)).setTextSize(int);

                //Interaction between database and buttons
                DatabaseReference myRef = FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
                //Get current user uId
                String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();



                switch (position){
                    case 0:
                        myRef.child("Users").child(authorId).child("radius").setValue(100000);
                        break;
                    case 1:
                        myRef.child("Users").child(authorId).child("radius").setValue(1);
                        Toast.makeText(getApplicationContext(),"Rayon = 1 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        myRef.child("Users").child(authorId).child("radius").setValue(5);
                        Toast.makeText(getApplicationContext(),"Rayon = 5 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        myRef.child("Users").child(authorId).child("radius").setValue(10);
                        Toast.makeText(getApplicationContext(),"Rayon = 10 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 4:
                        myRef.child("Users").child(authorId).child("radius").setValue(20);
                        Toast.makeText(getApplicationContext(),"Rayon = 20 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 5:
                        myRef.child("Users").child(authorId).child("radius").setValue(50);
                        Toast.makeText(getApplicationContext(),"Rayon = 50 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 6:
                        myRef.child("Users").child(authorId).child("radius").setValue(100);
                        Toast.makeText(getApplicationContext(),"Rayon = 100 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 7:
                        myRef.child("Users").child(authorId).child("radius").setValue(300);
                        Toast.makeText(getApplicationContext(),"Rayon = 300 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 8:
                        myRef.child("Users").child(authorId).child("radius").setValue(1000);
                        Toast.makeText(getApplicationContext(),"Rayon = 1000 km",Toast.LENGTH_SHORT).show();
                        break;
                    case 9:
                        myRef.child("Users").child(authorId).child("radius").setValue(100000);
                        Toast.makeText(getApplicationContext(),"Rayon = + 1000 km",Toast.LENGTH_SHORT).show();
                        break;
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                //If nothing is selected, default radius =100 000 km
                DatabaseReference myRef = FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
                //Get current user uId
                String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();
                myRef.child("Users").child(authorId).child("radius").setValue(100000);
                Toast.makeText(getApplicationContext(),"Rayon = +1000 km",Toast.LENGTH_SHORT).show();
            }
        });

    }

// ----------------------------------- Navigation Interactions -------------------------------------

    @Override
    public void onBackPressed(){
        //if back button is pressed: redirect back to main activity
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        //Precise the animation when switch activity
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        //Close the settings activity
        finish();
    }
}