package fr.tad.tousantidechet;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.android.gms.maps.GoogleMap.OnMarkerClickListener;

import java.util.ArrayList;
import java.util.List;

public class MapsActivityAdd extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    private static final String FINE_LOCATION = Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private FusedLocationProviderClient fusedLocationProviderClient;

    private boolean isLocationEnable = false;

    private LatLng currentLocation;
    private List<LatLng> announcesPositionList = new ArrayList<LatLng>();
    public static double lat, lng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_add);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);


        // Check permissions
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {


            ActivityCompat.requestPermissions(this, new String[]{FINE_LOCATION, COARSE_LOCATION},
                    LOCATION_PERMISSION_REQUEST_CODE);
            return;
        } else {
            isLocationEnable = true;


        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // if permissions granted get user location
        if (isLocationEnable) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            }
            fusedLocationProviderClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location. In some rare situations this can be null.
                            if (location != null) {
                                // Logic to handle location object
                                currentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                                // Center the map on the current location
                                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));
                                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                                } else {
                                    // Display a blue dot on the map on the current location
                                    mMap.setMyLocationEnabled(true);

                                }
                            }
                        }
                    });
        }

        // Load locally the database and display all the announces on the map
        AnnounceStorageManager announceStorageManager = AnnounceStorageManager.getInstance();
        for (Announce announce : announceStorageManager.getAnnounces()) {
            announcesPositionList.add(new LatLng(announce.getLatitude(), announce.getLongitude()));
            mMap.addMarker(new MarkerOptions().position(new LatLng(announce.getLatitude(), announce.getLongitude())).title(announce.getDescription()));
        }


        // Listen if click on the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng pos) {
                // Add a marker on the map
                MarkerOptions markerOptions =new MarkerOptions().position(pos).title("Click pour confirmer la position");
                markerOptions.draggable(true);
                mMap.addMarker(markerOptions);

            }
        });

        // Listen if click on any existing marker
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {

                // If the clicked marker is not an existing announce but a just created marker
                if (!announcesPositionList.contains(marker.getPosition())){
                    //Build a dialog window to confirm the position of the announce
                    // setting the details of the displayed window with AlertDialog.Builder methods
                    AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(MapsActivityAdd.this);

                    //set the title of the window
                    dialogBuilder.setTitle("Confirmer la position")
                            //set the message to display
                            .setMessage("Voulez vous confirmer cette position pour votre annonce?")
                            //set the text on the positive button then listen for a click on it
                            .setPositiveButton("Oui", new DialogInterface.OnClickListener(){
                                public void onClick(DialogInterface dialog,int i){
                                    // If user confirms:

                                    // Store the position in public static variables to get it from CreateAnnounceActivity
                                    lat=marker.getPosition().latitude;
                                    lng=marker.getPosition().longitude;

                                    //Display a message to inform the user
                                    Toast.makeText(getApplicationContext(), "La position de votre annonce a bien été enregistrée", Toast.LENGTH_SHORT).show();

                                    //redirect to CreateAnnounceActivity activity
                                    startActivity(new Intent(getApplicationContext(),CreateAnnounceActivity.class));
                                    //Precise animation
                                    overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                                    //close activity
                                    finish();
                                }
                            })
                            .setNegativeButton("Non",null)
                            .setIcon(R.drawable.alert_icon);

                    AlertDialog alertDialog = dialogBuilder.create();
                    alertDialog.show();

                }

                return false;
            }
        });
    }



    @Override
    public void onBackPressed(){
        //if back button is pressed: redirect back to main activity
        startActivity(new Intent(getApplicationContext(),CreateAnnounceActivity.class));
        //Precise the animation when switch activity
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        //Close the maps activity
        finish();
    }
}