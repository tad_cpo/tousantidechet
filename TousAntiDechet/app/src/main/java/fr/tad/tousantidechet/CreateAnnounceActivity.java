package fr.tad.tousantidechet;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static fr.tad.tousantidechet.MapsActivityAdd.lat;
import static fr.tad.tousantidechet.MapsActivityAdd.lng;

//This activity allows the user to create a new announce with graphic interaction:
// Text inputs for type and description (type could be changed to a spinner (list of choices))


public class CreateAnnounceActivity extends AppCompatActivity {

    private ImageView back_icon,map_icon,done_icon;
    private TextInputLayout type_input,description_input;
    private Button create_announce_button;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        //Link the graphic elements
        setContentView(R.layout.activity_create_announce);

        type_input=findViewById(R.id.type_input);
        description_input=findViewById(R.id.description_input);
        create_announce_button=findViewById(R.id.confirm_create_announce_button);
        map_icon=findViewById(R.id.map_icon);
        done_icon=findViewById(R.id.done_icon);



// --------------------------------- Interactions with graphic items -------------------------------

        //Listen if the map_icon is clicked
        map_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getApplicationContext(),MapsActivityAdd.class));
                overridePendingTransition(R.anim.slide_in_right,R.anim.slide_out_left);
                finish();
            }
        });

        // Check if a location has been chosen in MapsActivityAdd
        if(MapsActivityAdd.lat!=0 || MapsActivityAdd.lng!=0){
            done_icon.setVisibility(View.VISIBLE);
        }
        else {
            done_icon.setVisibility(View.INVISIBLE);
        }



        //Listen if the create_announce_button is clicked
        create_announce_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                type_input.setErrorEnabled(false);
                description_input.setErrorEnabled(false);

                //Get what the user entered in type and description text input
                String type = type_input.getEditText().getText().toString().trim();
                String description = description_input.getEditText().getText().toString().trim();

                //If the user did not enter type or description, set error with message to indicate the error
                if (TextUtils.isEmpty(type)) {
                    type_input.setError("Entrez un type");
                    return;
                }

                if (TextUtils.isEmpty(description)) {
                    description_input.setError("Entrez une description");
                    return;
                }

                if(MapsActivityAdd.lat==0 & MapsActivityAdd.lng==0){
                    Toast.makeText(getApplicationContext(),"Localisez votre annonce",Toast.LENGTH_SHORT).show();
                }
                else{

                DatabaseReference myRef= FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
                //Get current user uId
                String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();




                //Get the id of the current user
                DatabaseReference authorRef = myRef.child("Users").child(authorId).child("id");

                //Get the value at ref (here: current user name)
                String[] userId = new String[1];
                authorRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        userId[0] =dataSnapshot.getValue(String.class);
                        //Add the new announce in the database
                        Announce announce = new Announce(userId[0],type,description,MapsActivityAdd.lat,MapsActivityAdd.lng);
                        AnnounceStorageManager.addAnnounce(announce);
                        //Display a message
                        Toast.makeText(getApplicationContext(),"Votre annonce a bien été ajoutée",Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });

                //----------------------------------------------------------------------------------


                //Redirect automatically to main activity
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                //Precise the animation
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
                //Close activity
                finish();

            }
            }
        });


// ---------------------------------- Navigation Interactions --------------------------------------


        //Listen for click on back_icon
        back_icon=findViewById(R.id.back_icon);
        back_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //redirect back to main activity if click
                startActivity(new Intent(getApplicationContext(),MainActivity.class));
                //Precise animation when switching activity
                overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
                finish();
            }
        });

    }

    @Override
    public void onBackPressed(){
        MapsActivityAdd.lat=0;MapsActivityAdd.lng=0;
        //redirect back to main activity if click
        startActivity(new Intent(getApplicationContext(),MainActivity.class));
        //Precise animation when switching activity
        overridePendingTransition(R.anim.slide_in_up,R.anim.slide_out_down);
        finish();
    }
}