package fr.tad.tousantidechet;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

//This activity is the launcher activity, every time you open the application this activity starts
//But once you are logged in it brings you the main activity
//If you are not logged in yet but already registered, you can interact with the graphic items to log yourself in
//If you are not registered yet, you can navigate to the register activity

public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth myAuth;
    private TextInputLayout email_input,password_input;
    private Button login_button;
    private TextView forgotPassword_text, createAccount_text;
    private int back_pressed_counter;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Disable dark theme in the application
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

        //Link with the associated layout
        setContentView(R.layout.activity_login);

        //Get our authentication database instance to interact with it
        myAuth = FirebaseAuth.getInstance();

        //Get the graphic items to interact with them
        email_input = findViewById(R.id.email_input);
        password_input = findViewById(R.id.password_input);
        login_button = findViewById(R.id.login_button);
        forgotPassword_text = findViewById(R.id.forgotPassword_text);
        createAccount_text = findViewById(R.id.createAccount_text);


//----------------------- Test if you are already logged in ----------------------------------------

        //If you are already logged in on this device...
        if (myAuth.getCurrentUser() != null) {

            //... you are redirected automatically to main activity
            startActivity(new Intent(getApplicationContext(), MainActivity.class));

            //then close login activity
            finish();
        }


//----------------------- Interactions with the graphic items --------------------------------------


        //Listen for a click on the item login_button
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //When login button is clicked:
                back_pressed_counter = 0;

                //Get what the user entered in email and password text input
                String email = email_input.getEditText().getText().toString().trim();
                String password = password_input.getEditText().getText().toString().trim();

                email_input.setErrorEnabled(false);
                password_input.setErrorEnabled(false);


                //If the user did not enter password or email, set error with message to indicate the error
                if (TextUtils.isEmpty(email)) {
                    email_input.setError("Entrez votre email");
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    password_input.setError("Entrez votre mot de passe");
                    return;
                }
                //If password is to short, set another error
                if (password.length() < 6) {
                    password_input.setError("Le mot de passe doit contenir au moins 6 caractères");
                    return;
                }

                //If the user is already registered he can log in and will be remembered even if he close the application
                myAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //If the user has been found in the database:

                            //Display a message to inform the user that he has been logged in
                            Toast.makeText(LoginActivity.this, "Vous êtes connecté", Toast.LENGTH_SHORT).show();

                            //Once logged in the user can access the main activity
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            //Precise the animation
                            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
                            //Close login activity
                            finish();

                        } else {
                            //Display a message to inform the user that an error occurred
                            Toast.makeText(LoginActivity.this, "Erreur!" + task.getException().getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    }
                });
            }
        });


// ----------------------------------- Navigation Interaction --------------------------------------


        //If you click on create account...
        createAccount_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                back_pressed_counter = 0;
                //You are redirected to register activity
                startActivity(new Intent(getApplicationContext(), RegisterActivity.class));
                //Precise the animation
                overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
                //Close login activity
                finish();
            }
        });
    }

    //This one cannot be in the onCreate method
    @Override
    //Listen if back button is pressed
    public void onBackPressed(){
        //Every time the back button is pressed

        //You need to double press the back button to close the app (the counter is reset when you click elsewhere)
        back_pressed_counter++;
        if(back_pressed_counter==1){
            //If back is pressed once: display a message to explain you need to double press back to quit
            Toast.makeText(getApplicationContext(),"Appuyez encore pour quitter",Toast.LENGTH_SHORT).show();
        }

        if(back_pressed_counter==2){
            //If back is pressed twice: close the activity (close the app)
            finish();
        }
    }


}