package fr.tad.tousantidechet;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


class ViewHolder extends RecyclerView.ViewHolder{
    private TextView type,description,distance;
    private ImageView map_display_icon;

// ------------------------------------ Constructor ----------------------------------------------
      public ViewHolder(@NonNull View itemView) {

          super(itemView);
          this.map_display_icon = itemView.findViewById(R.id.map_display_icon);
          this.type = itemView.findViewById(R.id.announceType);
          this.description = itemView.findViewById(R.id.announceDescription);
          this.distance = itemView.findViewById(R.id.announceDistance);
      }


// --------------------------------- Getters -------------------------------------------------------
    public ImageView getMap_display_icon(){return map_display_icon;}

    public TextView getType() {
        return type;
    }

    public TextView getDescription() {
        return description;
    }

    public TextView getDistance() {
        return distance;
    }
}

