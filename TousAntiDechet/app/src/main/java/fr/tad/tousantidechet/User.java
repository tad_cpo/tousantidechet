package fr.tad.tousantidechet;


import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class User {

    private String name;
    private final String id; //not an int id because firebase provides a unique String uId
    private String age; //not an int because age is get by a spinner (can be modified)
    private String region;
    private boolean notifications_activity;
    private double radius;
    public static double mRadius;


// ------------------------------------- Constructors ---------------------------------------------

    public User() {
        this.id = null;
    } //need a default constructor to compile

    public User(String name, String id, String age, String region) {
        this.name = name;
        this.id = id;
        this.age = age;
        this.region = region;
        this.notifications_activity = true;
        this.radius=100000;
    }


// ---------------------------------------- Getters ------------------------------------------------

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getAge() {
        return age;
    }

    public String getRegion() {
        return region;
    }

    public boolean getNotifications_activity() {
        return notifications_activity;
    }

    public double getRadius() {
        return radius;
    }
    // ---------------------------------------- Setters ------------------------------------------------

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setNotifications_activity(boolean notifications_activity) {
        this.notifications_activity = notifications_activity;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    // ------------------------------------- announces methods ----------------------------------------

    //A user can delete only the announces he created
    public void deleteAnnounce(Announce announce) {

        if (announce.getAuthorId() == FirebaseAuth.getInstance().getCurrentUser().getUid()) {
            AnnounceStorageManager.deleteAnnounce(announce);
        }
    }


    public static void getCurrentUserRadius(){


        DatabaseReference myRef= FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference();
        //Get current user uId
        String authorId = FirebaseAuth.getInstance().getCurrentUser().getUid();

        //Get the radius of current user reference
        DatabaseReference authorRef = myRef.child("Users").child(authorId);

        //Get the value at ref (here: current user radius)
        double[] radius = new double[1];
        authorRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                radius[0] =dataSnapshot.child("radius").getValue(double.class);
                mRadius=radius[0];

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}