package fr.tad.tousantidechet;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

//This class is used to link the local storage and the User database (Firebase real time data base)
public class UserStorageManager {
    private List<User> users;
    private DatabaseReference myRef=FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference("Users");
    private static UserStorageManager userStorageManager;
    private OnCustomEventListener listener;


//-------------------------------------- Constructor -----------------------------------------------

    private UserStorageManager(){

        //create the local User array
        this.users = new ArrayList<User>();

    }

public static UserStorageManager getInstance(){
    if ( userStorageManager== null)
    {
        userStorageManager = new UserStorageManager();
    }
    return userStorageManager;
}

// --------------------------------------- Getters -------------------------------------------------

    public List<User> getUsers(){
        return this.users;
    }

    public DatabaseReference getMyRef() {
        return myRef;
    }


// ------------------------------ Set the Custom Listener ------------------------------------------

    public void setCustomEventListener(OnCustomEventListener eventListener){
        this.listener=eventListener;
    }

// ----------------------------- Database modification methods -------------------------------------

//These methods are static because they are needed in multiple activities,
// while the only instance of UserStorageManager is in the main activity (will be moved to SeeAnnouncesActivity)

    //Add a new user in the database
    public static void addUser(User user){

        FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference("Users").child(user.getId()).setValue(user);
    }

    //Delete a user in the database using his unique id
    public static void deleteUser(String uId){
        FirebaseDatabase.getInstance("https://tousantidechet-default-rtdb.firebaseio.com/").getReference("Users").child(uId).removeValue();
    }


// ------------------------------- Local Array modification methods --------------------------------

//the only useful method here is the updateUsers method because it is used only with an OnChangeListener on database
// so to keep the local array corresponding to the database no other method should be used

    public void updateUsers(DataSnapshot dataSnapshot){
        // the DataSnapshot type is from the OnChangeListener method

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                //Clear the local array to rewrite the entire database
                users.clear();

                //Get every items in the database
                for (DataSnapshot ds : dataSnapshot.getChildren()){
                    User value = ds.getValue(User.class);

                    //and add them in the local array
                    users.add(value);
                }
                //Call onEvent when the database has been successfully copied locally
                // onEvent method should be Override in the Activity updateUsers method is called
                if(listener!=null){
                    listener.onEvent();}
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

}
